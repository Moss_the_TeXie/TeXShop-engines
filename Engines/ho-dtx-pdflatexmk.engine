#!/bin/zsh
# This is a TeXShop engine supporting the Oberdiek mechanism:
# Running TeX on the DTX calls docstrip to produce the files, while
# *LaTeX gives the documentation.
# This iteration uses pdflatex.
# This file must go into ~/Library/TeXShop/Engines.
# -MWL- 2013-10-31, updated 2022-03-02
# -MWL- new version 2022-07-06
# -MWL- endjinn.log added 2022-07-18

BASENAME="$(basename ${1} .dtx)"
ENDJINN=${BASENAME}-endjinn.log

echo "TeXShop engine for the Oberdiek mechanism, using PDFLaTeX." > ${ENDJINN}
echo "parameters = $*" >> ${ENDJINN}

# create a zsh argument list from $2
xargarray=( ${(z)2} )
for ((i=1; i <= $#xargarray; i++ )); 
  do xargarray[$i]=${(Q)xargarray[$i]}; 
done;
 
echo "generating the payload" >> ${ENDJINN}

tex --file-line-error "$1"

echo "%%% begin ${BASENAME}.log %%%%%%%%%%%%%%%%%">> ${ENDJINN}
cat -nv "${BASENAME}.log" >> ${ENDJINN}
echo "%%% end ${BASENAME}.log %%%%%%%%%%%%%%%%%%%">> ${ENDJINN}
# secure the log of this TeX/docstrip run before it gets overwritten by the following manual generation run
# mv "${BASENAME}.log" "${BASENAME}-docstrip.log"

echo "generating the documentation" >> ${ENDJINN}

# for TeXShop 4.66+ only
export TSBIN="$HOME/Library/TeXShop/bin/tslatexmk"

#export LTMKBIN="$HOME/Library/TeXShop/bin/tslatexmk"
whereisit="`which latexmk`"
if [ "${whereisit}" != "latexmk not found" ] ; then
  echo "latexmk found: using ${whereisit}" >> ${ENDJINN}
  export LTMKBIN="`dirname ${whereisit}`"
else
  echo "latexmk not found: using internal default" >> ${ENDJINN}
  export LTMKBIN="${HOME}/Library/TeXShop/bin/tslatexmk"
fi 

export LTMKEDIT="$HOME/Library/TeXShop/bin"
# make sure latexmkrcedit exists in bin
if [ ! -e "${LTMKEDIT}/latexmkrcedit" ] ; then
	cp "${TSBIN}/latexmkrcDONTedit" "${LTMKEDIT}/latexmkrcedit"
fi
# Use local rc file platexmkrc if it exists. p = project. zsh version
export localrc=""
if [ -e ./platexmkrc ] ; then
  echo "using ./platexmkrc3" >> ${ENDJINN}
	export localrc=( -r ./platexmkrc )
fi

"${LTMKBIN}"/latexmk -pdflatex -r "${LTMKEDIT}/latexmkrcedit" ${xargarray} -r "${TSBIN}/ho-dtxmkrc" ${localrc} "$1"

echo "*** done ***" >> ${ENDJINN}
