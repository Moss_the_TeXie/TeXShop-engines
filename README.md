# TeXShop Engines

Here we have a few TeXShop engines.

Those with names ending in `-para` extend the engines supplied with TeXShop with the ability to process a second parameter, possibly containing several individual options, which is specified with the directive "%TEX parameter = ...".

Others are using/complementing the Oberdiek DTX mechanism to produce payload files and documentation in one go.  Most are a bit older or quite arcane (`akad*.engine`).  For new projects please use only the new ones: `ho-dtx-pdflatexmk.engine` and `ho-dtx-lualatexmk.engine`.

## Requirements
TeXShop 4.x, some TeX distrib (probably TeXlive), MacOS.

## Installation
Copy the Engines (all or just the ones to be used) from `./Engines` into `~/Library/TeXShop/Engines`.
Copy the relevant `latexmk` scripts (or just all of them) from `./bin/tslatexmk`to `~/Library/TeXShop/bin/tslatexmk/`.
If it does not work instantly, restart TeXShop.

## Usage
At the top of your Source file place the line
`% !TEX program = ho-dtx-lualatexmk`
to use the ho-dtx-lualatexmk engine which will use plain TeX to generate the files of your package from the DTX file and if that succeeds uses LuaLaTeX (and latexmk) to typeset the documentation.

For details see “Using latexmk with TEXShop” by Herbert Schulz, `~/Library/TeXShop/Engines/Inactive/Latexmk/Latexmk For TeXShop.pdf`.
Be advised that so far only the two engines `ho-dtx-pdflatexmk` and `ho-dtx-lualatexmk` accept local `platexmkrc` configurations.
Passing a second parameter (the first contains the file name) to the underlying typesetting engine using the
`% !TEX-parameter = ...` directive.
directive is supported by all non-obsolete engines, especially the `-para` scripts.  This can contain several command line options, which are separated internally and passed on to the binary to be called.

## Support
Just me, by now: `moss@tex.guru`

## Roadmap
None.  New engines will be updated as necessary (and as I see fit), older ones may or may not (again, as I see fit).

## Contributing
If anyone uses this stuff and has comments, issues, whatever, please feel free to mention it.  No garanties, though.

## Authors and acknowledgment
Lots and lots of thanks and appreciation to the people behind and around TeXShop, namely Richard Koch and Herbert Schulz.

## License

Copyright 2022 Martin Wilhelm Leidig

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3c
of this license or (at your option) any later version.
The latest version of this license is in

`http://www.latex-project.org/lppl.txt`

and version 1.3c or later is part of all distributions of LaTeX
version 2008/05/04 or later.

This work has the LPPL maintenance status `maintained`.

The Current Maintainer of this work is Martin Wilhelm Leidig.

## Project status
Active, more or less.  Engines are adapted to new TeXShop versions and other developments as needed (by me).